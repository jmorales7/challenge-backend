import express, { Request, Response } from 'express';
import axios from 'axios';
import fs from 'fs';
import sha1 from 'sha1';

interface AgeItem {
  key: string;
  age: number;
}

const fetchData = async (): Promise<AgeItem[]> => {
    const response = await axios.get('https://coderbyte.com/api/challenges/json/age-counting');
    const dataString: string = response.data.data;
    const items: AgeItem[] = [];

    const keyValuePairs = dataString.split(', ');
    for (let i = 0; i < keyValuePairs.length; i += 2) {
        const key = keyValuePairs[i].split('=')[1]; // Extract the key
        const age = parseInt(keyValuePairs[i + 1].split('=')[1], 10); // Extract the age and convert to a number

        // Check if age is a valid number, and if both key and age are present, add the AgeItem to the items array
        if (!isNaN(age) && key) {
        items.push({ key, age });
        }
    }
    return items;
  };

const countAge32Items = (items: AgeItem[]): number => {
  return items.reduce((count, item) => (item.age === 32 ? count + 1 : count), 0);
};

const writeKeysToFile = (items: AgeItem[]): void => {
  const keys = items.filter((item) => item.age === 32).map((item) => item.key);
  fs.writeFileSync('output.txt', keys.join('\n') + '\n');
};

const calculateSHA1Hash = (filename: string): string => {
  const fileContents = fs.readFileSync(filename, 'utf-8');
  return sha1(fileContents);
};

const app = express();

app.get('/api/process-age-data', async (req: Request, res: Response) => {
  try {
    const items = await fetchData();
    const age32Count = countAge32Items(items);
    writeKeysToFile(items);
    const hash = calculateSHA1Hash('output.txt');

    res.json({
      numberOfItemsWithAge32: age32Count,
      sha1HashOfFile: hash,
    });
  } catch (error) {
    console.log(error)
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
